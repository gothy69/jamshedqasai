<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\ImageManagerStatic as Image;
use \Analytics;
use Spatie\Analytics\Period;


class BlogController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBlogs = Blog::paginate(3);
//        Blog::deleteIndex();
//        Blog::addAllToIndex();
//        $allBlogs = Blog::with('postedBy')->get();
        return view('mainPage', compact('allBlogs'));
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $blogs = Blog::searchByQuery(array('match_phrase_prefix' => array('title' => $request->search)));
//            where('title', 'LIKE', '%' . $request->search . "%")->get();
            if ($blogs) {
                foreach ($blogs as $key => $blog) {
                    $output .=
                        '<a href=" /blogPost/' . $blog->id . '" style="text-decoration:none">' . $blog->title . '</a><br/>';
                }
                return $output;
            }
            else
            {
                $output .=
                    '<p>No Search Results</p>';
                return $output;
            }
        }
    }

    public function blogPost($id)
    {
        $blog = Blog::findOrFail($id);
        $comment = Comment::where('blog_id', $id)->get();

        return view('samplePost', compact('blog', 'comment'));
    }

    public function blogabout()
    {
        return view('about');
    }

    public function blogContact()
    {
        return view('contact');
    }

    public function editProfile($id)
    {

        $user = User::findOrFail($id);
        return view('editProfile', compact('user'));
    }

    public function update(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        if (Hash::check($request->oldpassword, $user->password)) {
            $user->name = $request->name;
            $user->email = $request->email;
            if($request->newpassword) {
                $hashednewpassword = Hash::make($request->newpassword);
                $user->password = $hashednewpassword;
            }

            if ($request->has("pic")) {
                $this->validate($request, [
                    'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
                ]);

                $file = Input::file('pic');
                $fileName = str_random() . $file->getClientOriginalName();

                $image_resize = Image::make($file->getRealPath());
                $image_resize->resize(100, 100);
                $path = 'uploads/'.$fileName;

                $image_resize->save($path);

                if (File::exists($user->path)) {
                    File::delete($user->path);
                }
                $user->path = 'uploads/' . $fileName;
            } elseif(Input::has('')) {
                $imagePath = $user->path;
                if (File::exists($imagePath)) {
                    File::delete($imagePath);
                    $user->path = null;
                }
            }
            $user->save();

            return redirect()->back()->with('message','Edit Successful!');
        } else {
            return 'Old Password is wrong';
        }
    }


    public function addComment(Request $request){

        $user = Auth::user()->name;
        $image = Auth::user()->path;
        $comment = new Comment;
        $comment ->user_id = Auth::user()->id;
        $comment ->blog_id = $request->blog_id;
        $comment -> comment = $request->comment;
        if($comment->save()){
            return response()->json(['status'=>200,'comment'=>$comment,'user'=>$user,'image'=>$image]);
        }else {

            return response()->json(['status' => 0]);
        }
    }

    public function send(Request $request)
    {
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'messagetext' => $request->message
        );
        Mail::send('contact_form', $data, function ($message) use ($request){
            /* Config ********** */

            $subject  = "Blog Query";
            $message->subject ($subject);
            $message->from ('hamzaghouri007@gmail.com', 'UNCLE MAJBOOR');
            $message->to ($request->email, $request->name);
        });

        if(count(Mail::failures()) > 0){
            $status = 'error';
        } else {
            return response()->json(['status'=>200]);
        }

        return true;
    }

    public function stripe(){

        return view ('stripe');
    }

    public function payment(Request $request){
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey("sk_test_P1c3Ro78avUFs0Qn0SxZH9yM");

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];
        $charge = \Stripe\Charge::create([
            'amount' => 999,
            'currency' => 'usd',
            'description' => 'Example charge',
            'source' => $token,
        ]);

        return Redirect::route('home');
    }

    public function setElastic(Request $request){
//        Blog::createIndex($shards = null, $replicas = null);
//        Blog::putMapping($ignoreConflicts = true);
        Blog::addAllToIndex();
//        Blog::rebuildMapping();
//        Blog::reindex();
//
//
//        $blog = Blog::orderBy("id", "desc")->first();
//        $blog->addToIndex();
        return "done";
    }

}
