<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin_UI.layouts.head')
</head>
<body class="hold-transition sidebar-mini">

<div class="wrapper">
@include('admin_UI.layouts.admin_navbar')
@include('admin_UI.layouts.admin_sidebar')

    <main>
        @yield('content')
    </main>


    @include('admin_UI.layouts.admin_footer')

</div>
@include('admin_UI.layouts.admin_scripts')
</body>