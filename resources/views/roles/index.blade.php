@extends('admin_UI.layouts.master')

@section('title', '| Users')
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <h1><i class="fa fa-users"></i> Admin </h1><br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>Role Name</th>
                        <th>Operations</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{$role->name}}</td>
                        <td><a href="{{route('edit_Role',$role->id)}}"><button type="button"
                                                                              class="btn btn-dark" name="edit">Edit</button></a>
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection